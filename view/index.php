<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="static/index.css">
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <title>Document</title>
</head>

<body>
  <div class="container">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th id="orderUser" class="sort" scope="col">Пользователь</th>
          <th id="orderEmail" class="sort" scope="col">Email</th>
          <th id="orderTask" class="sort" scope="col">Статус</th>
          <th scope="col">TaskText</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($tasks as $value) : ?>
          <tr>
            <td><?php echo $value->getUserName(); ?></td>
            <td><?php echo $value->getEmail(); ?></td>
            <?php if ($value->getIsDone()) : ?>
              <td> <span class="badge badge-success">Выполненно</span></td>
            <?php else : ?>
              <td> <span class="badge badge-secondary">В процессе</span></td>
            <?php endif; ?>

            <td id="task-text<?php echo $value->getId(); ?>"><?php echo $value->getTaskText(); ?></td>

            <?php if (isset($_SESSION['logined']) && $_SESSION['logined']) : ?>
              <td><button type="button" data-id="<?php echo $value->getId(); ?>" class="btn btn-primary btn-sm edtask">Изменить</button>
                <?php if (!$value->getIsDone()) : ?>
                  <button type="button" data-id="<?php echo $value->getId(); ?>" class="btn btn-secondary btn-sm edtaskDone">Выполнена</button>
                <?php endif; ?>

              </td>
            <?php endif; ?>

            <?php if ($value->getIsAdminEdited()) : ?>
              <td id="label<?php echo $value->getId(); ?>"><span class="badge badge-pill badge-danger">Изменил администратор</span></td>
            <?php else : ?>
              <td id="label<?php echo $value->getId(); ?>"></td>
            <?php endif; ?>

          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <ul class="pagination justify-content-center">

      <?php for ($i = 1; $i <= $pages; $i++) { ?>
        <?php if ($i == $page) : ?>
          <li class="page-item active"><a class="page-link" href="?page=<?php echo $i ?>"><?php echo $i ?></a></li>
        <?php else : ?>
          <li class="page-item"><a class="page-link" href="?page=<?php echo $i ?>"><?php echo $i ?></a></li>
        <?php endif; ?>

      <?php } ?>
    </ul>
    <form method="POST" action="/" class="form-inline">
      <div class="form-group mb-2">
        <label for="username" class="sr-only">Имя пользователя</label>
        <input type="text" class="form-control" name="username" id="username" placeholder="Имя пользователя">
      </div>
      <div class="form-group mx-sm-3 mb-2">
        <label for="email" class="sr-only">Email</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
      </div>
      <div class="form-group mx-sm-3 mb-2">
        <label for="task" class="sr-only">Текст задачи</label>
        <input type="text" class="form-control" name="task" id="task" placeholder="Задача">
      </div>
      <button type="submit" class="btn btn-primary mb-2">Добавить</button>
    </form>

    <form method="POST" action="/" class="form-inline">
      <div class="form-group mb-2">
        <label for="user" class="sr-only">admin</label>
        <input type="text" class="form-control" name="user" id="user" placeholder="admin">
      </div>
      <div class="form-group mx-sm-3 mb-2">
        <label for="password" class="sr-only">password</label>
        <input type="password" class="form-control" name="password" id="password" placeholder="password">
      </div>
      <button type="submit" class="btn btn-primary mb-2">Войти</button>
    </form>


  </div>
  <script src="static/app.js"></script>
</body>

</html>