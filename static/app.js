const ORDER_USER_NAME = 'user_name';
const ORDER_EMAIL = 'email';
const ORDER_DONE = 'done';
document.querySelectorAll('.page-link').forEach(button => updateNavigation(button));

function updateNavigation(button) {
    let buttonHref = button.getAttribute('href');
    let argsButton = parseLocation(buttonHref);
    let args = parseLocation(document.location.search);
    if (args['sort']) {
        let href = `?page=${argsButton['page']}&sort=${args['sort']}`;
        button.setAttribute('href', href);
    }
}
document.querySelectorAll('.edtask').forEach(button => {
    button.addEventListener('click', (event) => {
        let text = prompt("Описание задачи", document.getElementById('task-text' + id).textContent);
        let htmlEdit = '<span class="badge badge-pill badge-danger">Изменил администратор</span>';
        let id = button.dataset.id;
        document.getElementById('task-text' + id).textContent = text;
        document.getElementById('label' + id).innerHTML = htmlEdit;
        let data = new FormData();
        data.append('id', id);
        data.append('text', text);

        fetch('/', {
            method: 'POST',
            body: data
        }).catch((err) => console.error(err));

    })
});

document.querySelectorAll('.edtaskDone').forEach(button => {
    button.addEventListener('click', (event) => {
        let data = new FormData();
        let id = button.dataset.id;
        data.append('id', id);
        data.append('done', 1);
        fetch('/', {
            method: 'POST',
            body: data
        }).catch((err) => console.error(err));
        location.reload();
    })
});

function parseLocation(querystring) {
    querystring = querystring.substring(querystring.indexOf('?') + 1).split('&');
    var params = {}, pair, d = decodeURIComponent;
    for (var i = querystring.length - 1; i >= 0; i--) {
        pair = querystring[i].split('=');
        params[d(pair[0])] = d(pair[1] || '');
    }
    return params;
};

document.getElementById('orderUser').addEventListener('click', (event) => {
    changeLocation('user_name');
});

document.getElementById('orderEmail').addEventListener('click', (event) => {
    changeLocation('email');

});

document.getElementById('orderTask').addEventListener('click', (event) => {
    changeLocation('done');
});

function changeLocation(sortType) {
    let args = parseLocation(document.location.search);
    if (args['page'] == undefined) {
        document.location.href += '?page=1' + '&sort=' + sortType;
        return;
    }
    if (args['page'] && !args['sort']) {
        document.location.href += '&sort=' + sortType;
    } else {
        document.location.href = `${document.location.protocol}//${document.location.host}/?page=${args['page']}&sort=${sortType}`;
    }
}