<?php
/*
Задачи состоят из:
- имени пользователя;
- е-mail;
- текста задачи;
Выполненные задачи 
в общем списке выводятся с соответствующей отметкой "отредактировано администратором".
*/
require_once('DB.php');
const ORDER_USER_NAME = 'user_name';
const ORDER_EMAIL = 'email';
const ORDER_DONE = 'done';

class Task
{
    protected $id;
    protected $userName;
    protected $email;
    protected $taskText;
    protected $isAdminEdited;
    protected $isDone;

    function getUserName()
    {
        return $this->userName;
    }
    function getId()
    {
        return $this->id;
    }
    function getEmail()
    {
        return $this->email;
    }
    function getTaskText()
    {
        return $this->taskText;
    }

    function getIsAdminEdited()
    {
        return $this->isAdminEdited;
    }

    function getIsDone()
    {
        return $this->isDone;
    }


    function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    function setTaskText($taskText)
    {
        $this->taskText = $taskText;
        return $this;
    }

    function setIsAdminEdited($adminEdited)
    {
        $this->isAdminEdited = $adminEdited;
        return $this;
    }

    function setIsDone($done)
    {
        $this->isDone = $done;
        return $this;
    }

    private function __construct($id, $userName, $email, $taskText, $isAdminEdited, $isDone)
    {
        $this->id =  intval($id);
        $this->userName = $userName;
        $this->email = $email;
        $this->taskText = $taskText;
        $this->isAdminEdited = intval($isAdminEdited);
        $this->isDone = intval($isDone);
    }

    static public function CreateTask($userName, $email, $taskText)
    {
        return new Task(null, $userName, $email, $taskText, 0, 0);
    }

    public function save()
    {
        $sql = 'INSERT into task(user_name,email,task_text,admin_edited, done) VALUES(?,?,?,?,?)';
        $conn = DB::getConnect();

        $stmt = $conn->prepare($sql);
        $stmt->execute([$this->userName, $this->email, $this->taskText, $this->isAdminEdited, $this->isDone]);
        return new Task($conn->lastInsertId(), $this->userName, $this->email, $this->taskText, $this->isAdminEdited, $this->isDone);
    }

    static public function getById($id)
    {
        $sql = "SELECT id,user_name,email,task_text,admin_edited, done FROM task WHERE id = ?";
        $conn = DB::getConnect();

        $stmt = $conn->prepare($sql);
        if ($stmt->execute(([$id]))) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return new Task($result['id'], $result['user_name'], $result['email'], $result['task_text'], $result['admin_edited'], $result['done']);
        }
        return null;
    }
    static public function getAll($orderBy = null, $offset = 0, $limit = 3)
    {
        $conn = DB::getConnect();
        //$orderBy = $conn->quote($orderBy)->st;
        if ($orderBy != null) {
            if (strcmp($orderBy, 'done') == 0) {
                $sql = "SELECT id,user_name,email,task_text,admin_edited, done FROM task ORDER BY " . $orderBy . " DESC LIMIT :limit OFFSET :offset";
            } else {
                $sql = "SELECT id,user_name,email,task_text,admin_edited, done FROM task ORDER BY " . $orderBy . " LIMIT :limit OFFSET :offset";
            }
        } else {
            $sql = "SELECT id,user_name,email,task_text,admin_edited, done FROM task LIMIT :limit OFFSET :offset";
        }
        $stmt = $conn->prepare($sql);
        $result = array();
        $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $data =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach ($data as $value) {
                array_push($result, new Task(
                    $value['id'],
                    $value['user_name'],
                    $value['email'],
                    $value['task_text'],
                    $value['admin_edited'],
                    $value['done']
                ));
            }
        }
        return $result;
    }
    static public function getPageCount($limit = 3)
    {
        $sql = "SELECT count(id) as 'count' FROM task";
        $conn = DB::getConnect();
        foreach ($conn->query($sql, PDO::FETCH_ASSOC) as $row) {
            return intval(ceil(intval($row['count']) / $limit));
        }
        return 0;
    }

    static public function remove($id)
    {
        $sql = "DELETE FROM task WHERE id = :id";
        $conn = DB::getConnect();
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->rowCount() == 1;
    }

    public function update()
    {
        $sql = 'UPDATE task SET user_name = :username, email = :email, task_text = :tasktext, admin_edited = :adminedited, done = :done WHERE id = :id';
        $conn = DB::getConnect();
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(":username", $this->userName, PDO::PARAM_STR);
        $stmt->bindValue(":email", $this->email, PDO::PARAM_STR);
        $stmt->bindValue(":tasktext", $this->taskText, PDO::PARAM_STR);
        $stmt->bindValue(":adminedited", true, PDO::PARAM_BOOL);
        $stmt->bindValue(":done", $this->isDone == 1, PDO::PARAM_BOOL);
        $stmt->bindValue(":id", $this->id, PDO::PARAM_INT);
        return $stmt->execute();
    }
}
