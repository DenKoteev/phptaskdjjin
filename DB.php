<?php

class DB
{
	private static $instance = NULL;

	function __construct()
	{
	}

	public static function  getConnect()
	{
		if (!isset(self::$instance)) {
			$servername = "localhost";
			$username = "id12667236_userdb";
			$password = "password";
			$database = "id12667236_taskdb";

			try {
				$conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
				// set the PDO error mode to exception
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				self::$instance = $conn;
			} catch (PDOException $e) {
				echo $e->getMessage();
			}
		}
		return self::$instance;
	}
}
