<?php
require_once('./model/task.php');
if (isset($_POST['user']) && isset($_POST['password'])) {
    $user = $_POST['user'];
    $password = $_POST['password'];
    if (strcmp($user, 'admin') == 0 && strcmp($password, '123')  == 0) {
        $_SESSION['logined'] = true;
    } else {
        $_SESSION['logined'] = false;
    }
}
if (isset($_POST['id']) && isset($_POST['text'])) {
    if ($_SESSION['logined']) {
        $task = Task::getById(intval($_POST['id']));
        $task->setTaskText($_POST['text']);
        $task->update();
    }
    exit;
}
if (isset($_POST['id']) && isset($_POST['done'])) {
    if ($_SESSION['logined']) {
        $task = Task::getById(intval($_POST['id']));
        $task->setIsDone(intval($_POST['done']) == 1);
        $task->update();
    }
}
if (isset($_POST['username']) && isset($_POST['email']) && isset($_POST['task'])) {
    $userName = $_POST['username'];
    $email = $_POST['email'];
    $task = $_POST['task'];
    Task::CreateTask($userName, $email, $task)->save();
}
$pages = Task::getPageCount();
$page = !isset($_GET['page']) ? 1 : $_GET['page'];
$tasks = null;
if (isset($_GET['sort']) && isset($_GET['page'])) {
    $sort = $_GET['sort'];
    $tasks = Task::getAll($sort, ($page - 1) * 3);
} else if (isset($_GET['sort'])) {
    $sort = $_GET['sort'];
    $tasks = Task::getAll($sort, 0);
} else if (isset($_GET['page'])) {
    $tasks = Task::getAll(null, ($page - 1) * 3);
} else {
    $tasks = Task::getAll(null, 0);
}
require_once('./view/index.php');
